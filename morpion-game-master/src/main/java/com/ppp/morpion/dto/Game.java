package com.ppp.morpion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date",
        "yourSymbole",
        "state"
})

@Getter
@Setter
public class Game {

    @JsonProperty("date")
    private String date;

    @JsonProperty("yourSymbole")
    private String yourSymbole;

    @JsonProperty("state")
    private List<String> state = new ArrayList<>();

}
