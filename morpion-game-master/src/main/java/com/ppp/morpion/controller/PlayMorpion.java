package com.ppp.morpion.controller;

import com.ppp.morpion.dto.Game;
import com.ppp.morpion.service.client.CallBotActionService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class PlayMorpion {


    private final CallBotActionService callBotActionService;

    public PlayMorpion(CallBotActionService callBotActionService) {
        this.callBotActionService = callBotActionService;
    }


    @GetMapping("/playPosition")
    public ResponseEntity<String> playPosition(@RequestParam(name="choice") String choice, @RequestParam(name="state[]") List<String> state){

        System.out.println("Position jouée : " + choice);
        System.out.println("Etat : " + state);

        Game currentGame = buildGameView(state);

        int botPositionPlayed = callBotActionService.getPositionToPlay(currentGame);

        return ResponseEntity.ok("C" + botPositionPlayed);
    }

    /**
     * Permet de construire une representation du jeux
     * @param state
     * @return
     */
    private Game buildGameView(List<String> state) {
        Game currentGame = new Game();

        currentGame.setYourSymbole("O");

        for (String currentCell:state) {
            currentGame.getState().add(currentCell);
        }

        return currentGame;
    }

}
