package com.ppp.morpion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MorpionGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(MorpionGameApplication.class, args);
    }

}
