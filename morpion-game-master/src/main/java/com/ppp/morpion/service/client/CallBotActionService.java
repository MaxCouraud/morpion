package com.ppp.morpion.service.client;

import com.ppp.morpion.dto.Game;

public interface CallBotActionService {

    /**
     * Permet de demander au bot de jouer son tour
     * @param currentGame
     * @return position
     */
    int getPositionToPlay(Game currentGame);
}
