package com.ppp.morpion.service.client.impl;

import com.ppp.morpion.dto.Game;
import com.ppp.morpion.service.client.CallBotActionService;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("callBotActionService")
public class CallBotActionServiceImpl implements CallBotActionService {

    private final Environment env;
    private final RestTemplate restTemplate;

    public CallBotActionServiceImpl(Environment env, RestTemplate restTemplate) {
        this.env = env;
        this.restTemplate = restTemplate;
    }

    @Override
    public int getPositionToPlay(Game currentGame) {
        return restTemplate.postForObject(env.getProperty("bot.server.url"), currentGame, Integer.class);
    }
}
